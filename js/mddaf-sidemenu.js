(function(){
    MDDAF.$sidemenu = {
        ctrl:function($scope){
            $scope.element = function(){
                return document.getElementById('sidemenu');
            };
            $scope.menu = {
                items:_m.config.menu.navList
            }
            $scope.nav = function(view){

                _m.$indicator.loading(function(){
                    _m.Router.nav.go2(view,(_m.app[view].hasData)?_m.app[view].hasData:false);
                });

            }
            //check initial visibility
            $scope.visible = !(_jQ($scope.element()).css("left").split('px')[0] < 0);

            //show menu
            $scope.show = function(){
                if($scope.visible != true){
                    _v($scope.element(),{left:'0%'},{duration:400});
                    $scope.visible = true;
                    _jQ("#sidemenu").trigger("opened");
                }
            }

            //hide menu
            $scope.hide = function(){
                if($scope.visible == true){
                    _v($scope.element(),{left:'-80%'},{duration:300});
                    $scope.visible = false;
                    _jQ("#sidemenu").trigger("closed");
                }
            }

            //pass reference
            _ang.sidemenu = $scope;
        }
    }
})();
