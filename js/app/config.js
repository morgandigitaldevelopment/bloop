(function(){
    MDDAF.config = {
        apiEndPt:"http://io.h5m.clients.morgandigitaldevelopment.com/",
        menu:{
            navList:[
                {id:1,label:'Hi 5 Meds',view:'main',icon: 'home'},
                {id:2,label:'View Menu',view:'menu',icon:'leaf'},
                {id:3,label:"Your Cart",view:'order',icon:'cart'},
                {id:4,label:'Sign Up',view:'signup',icon:'user'}
            ]
        },
        app:{
            cartDataKey:'h5m-cart',
            menuCacheKey:'h5m-menu',
            menuDBKey:'h5m-menu-db'
        }
    }
})();
