(function(){
    MDDAF.app.main ={
        hasData:false,
        init:function(){
            var vid = document.getElementsByClassName('fullvid')[0];
            vid.play();
            this.bindings();
        },
        bindings:function(){
            _jQ("#gen-btn").on("click",function(){
                _m.Router.nav.go2("bloopgen");
            });
            _jQ("#listen-btn").on("click",function(){
                window.open("http://dashradio.com","_system");
            })
            _jQ(".fullvid").on("canplay",function(){
                _jQ("#preload").fadeOut(function(){
                    _jQ(".fullvid").fadeIn();
                });
            })
            _jQ("#mute").on("click",function(){
                _jQ(".fullvid").prop("muted",!_jQ(".fullvid").prop("muted"));
                if(_jQ(".fullvid").prop("muted")){
                    _jQ("#mute > i").removeClass("sti-volume1");
                    _jQ("#mute > i").addClass("sti-volume2");
                }else{
                    _jQ("#mute > i").removeClass("sti-volume2");
                    _jQ("#mute > i").addClass("sti-volume1");
                }
            });
        }
    }
})();
