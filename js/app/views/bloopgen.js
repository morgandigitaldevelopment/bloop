(function(){
    MDDAF.app.bloopgen ={
        hasData:false,
        init:function(){
            this.bindings();
        },
        bindings:function(){
            _jQ("#bm1").on("click",function(){
                MDDAF.app.bloopgen.loadBiList(1)
            });
            _jQ("#bm2").on("click",function(){
                MDDAF.app.bloopgen.loadBiList(2)
            });
            _jQ("#back-btn").on("click",function(){
                _m.Router.nav.go2("main");
            });

            _jQ("#share-btn").on("click",function(){

                MDDAF.app.bloopgen.render(function(bloop){
                    MDDAF.app.bloopgen.share(bloop);
                });
            });

            _jQ("#save-btn").on("click",function(){
                // html2canvas(_jQ("#bloop"), {
                //     onrendered: function(canvas) {
                //         // canvas is the final rendered <canvas> element
                //         var myImage = canvas.toDataURL("image/png");
                //         MDDAF.app.bloopgen.save(myImage);
                //     },
                //     allowTaint:true,
                //     useCORS:true
                // });
                MDDAF.app.bloopgen.render(function(bloop){
                    MDDAF.app.bloopgen.save(bloop);
                });
            });
        },
        save:function(bloop){
            MDDAF.app.bloopgen.render(function(bloop){
                var params = {data: bloop.split(";base64,")[1], prefix: 'bloop_', format: 'PNG', quality: 100, mediaScanner: true};
                    window.imageSaver.saveBase64Image(params,
                        function (filePath) {
                            MDDAF.$indicator.success("Image Saved");
                        },
                        function (msg) {
                            MDDAF.$indicator.error(msg);
                        }
                    );
            });
        },
        share:function(bloop){
            // this is the complete list of currently supported params you can pass to the plugin (all optional)
            var options = {
              message: 'Created with the official ✨bloop✨ Generator [http://bit.ly/bloopApp]', // not supported on some apps (Facebook, Instagram)
              subject: '✨bloop✨', // fi. for email
              files: [bloop], // an array of filenames either locally or remotely
              url: 'http://bit.ly/bloopApp',
            //   chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
            }

            var onSuccess = function(result) {
              console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
              console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
            }

            var onError = function(msg) {
              console.log("Sharing failed with message: " + msg);
            }

            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
        },
        render:function(cb){
            var scaleBy = 2;
            var div = _jQ('#bloop');
            var w = _jQ('#bloop').width();
            var h = _jQ('#bloop').width();
            var canvas = document.createElement('canvas');
            canvas.width = w * scaleBy;
            canvas.height = h * scaleBy;
            canvas.style.width = w + 'px';
            canvas.style.height = h + 'px';
            var context = canvas.getContext('2d');
            context.fillStyle = "#ecf0f1";
            context.fillRect(0,0,(w * scaleBy),(h * scaleBy));
            context.scale(scaleBy, scaleBy);

            html2canvas(_jQ("#bloop"), {
                canvas:canvas,
                onrendered: function (canvas) {
        			cb(canvas.toDataURL("image/png"));
                }
            });
        },
        setBloopmoji:function(bm,bmi){
            _jQ(bm+" > img").attr("src",this.data.bmiLocPrefix+bmi+".png");
            this.hideBiList();
        },
        listShowing:false,
        loadBiList:function(bloopmoji){
            var list = "<ul id='bloopmoji'>";
                list += "{{#bloopmojis}}";
                list += "{{#.}}<li onclick=\"MDDAF.app.bloopgen.setBloopmoji('#bm"+bloopmoji+"','{{img}}')\" class='bloop-li'>";
                list += "<img src='{{{bmiLocPrefix}}}{{img}}.png' class='bloopmoji' />";
                list += "</li>{{/.}}";
                list += "{{/bloopmojis}}"
                list += "</ul>";
            _jQ("#bloopmojis").html(Mustache.render(list,this.data));
            _jQ("#bloopmojis").fadeIn(function(){
                MDDAF.app.bloopgen.listShowing = true;
            });
        },
        hideBiList:function(){
            _jQ("#bloopmojis").fadeOut(function(){
                MDDAF.app.bloopgen.listShowing = false;
            });
        },
        data:{
            bmiLocPrefix:"./res/img/pngs/icons/BLOOP_",
            bloopmojis:[
                {img:"1Hunnid"},
                {img:"Angel"},
                {img:"Barf"},
                {img:"Bling"},
                {img:"Bomb"},
                {img:"Clover"},
                {img:"Clown"},
                {img:"Devil"},
                {img:"Dove"},
                {img:"Earth"},
                {img:"EggPlant"},
                {img:"EggPlantFlipped"},
                {img:"FaceSmoke"},
                {img:"FingerPoint"},
                {img:"Fire"},
                {img:"GreenHeart"},
                {img:"HandOK"},
                {img:"LaughFace"},
                {img:"LipBite"},
                {img:"MadFace"},
                {img:"MicHand"},
                {img:"PraiseHands_Black"},
                {img:"PraiseHands_White"},
                {img:"MiddleFinger_Black"},
                {img:"MiddleFinger_White"},
                {img:"Peach"},
                {img:"PraiseHands_Black"},
                {img:"SadJordan"},
                {img:"Shit"},
                {img:"Shroom"},
                {img:"SoloCup"},
                {img:"StressFace"},
                {img:"TearsFace"},
                {img:"TheEnd"},
                {img:"TRUMP"},
                {img:"TrumpProfile"},
                {img:"USFlag"},
                {img:"USFlagBrown"},
                {img:"Weed"},
                {img:"Wet"},
                {img:"BlackPunch"},
                {img:"WhitePunch"},
            ]
        }
    }
})();
