(function(){
	MDDAF._HELPERS = {
		elm:function(elementName){

			var jqElm = _jQ("#"+elementName),
				nativeElm = document.getElementById(elementName);

			return {jq:jqElm,js:nativeElm}
		},
		elms:function(){

		},
		files:{
			size:function(bytes) {
				return (b=Math,c=b.log,d=1e3,e=c(bytes)/c(d)|0,bytes/b.pow(d,e)).toFixed(2)+' '+(e?'kMGTPEZY'[--e]+'B':'Bytes');
			}
		},
		getParam:function(param){
			var result = "Not found",
				tmp = [];
			var items = location.search.substr(1).split("&");
			for (var index = 0; index < items.length; index++) {
				tmp = items[index].split("=");
				if (tmp[0] === param) result = decodeURIComponent(tmp[1]);
			}
			return result;
		},
		promise:function(doThis,testThis,success,fail){

			var promise = new Promise(function(resolve, reject) {

			  if (testThis) {
				resolve({ok:true});
			  }
			  else {
				reject(Error("It broke"));
			  }
			});
			promise.then(function(result){
				success(result);
				doThis();
			},
			function(error){
				fail(error);
			});
		},
		curry:function(fn){
		   var arity = fn.length;

	       function given (argsSoFar) {
	           return function helper () {
	               var args             = Array.prototype.slice.call(arguments, 0);
	               var updatedArgsSoFar = argsSoFar.concat(args);

	               if (updatedArgsSoFar.length >= arity) {
	                   return fn.apply(this, updatedArgsSoFar);
	               }
	               else {
	                   return given(updatedArgsSoFar);
	               }
	           }
	       }

	       return given([]);
	   }
	}
})();
(function(){
	Date.prototype.toDateInputValue = (function() {
	    var local = new Date(this);
	    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
	    return local.toJSON().slice(0,10);
	});
	Date.prototype.toTimeInputValue = (function(){ //set time to now
		var d = new Date(),
			h = d.getHours(),
			m = d.getMinutes();
		if(h < 10) h = '0' + h;
		if(m < 10) m = '0' + m;
		// $(this).attr({
		//   'value': h + ':' + m
		// });
		return h + ':' + m;
	  });
})();
