//Globals
var _LOC = "./",
	_VIEWLOC = "./views/",
	_ang = {},
	$ang,
	_jS,
	_jQ,
	_v,
	_m;

(function(){
//Load Deps

	var deps = [
		//JS Deps
		_LOC+"lib/js/jquery.min.js",
		_LOC+"lib/js/angular.min.js",
		_LOC+"lib/js/mustache.min.js",
		_LOC+"lib/js/underscore.js",
		_LOC+"lib/js/html2canvas.js",
		//MDDAF
		_LOC+"js/mddaf.js",
		_LOC+"js/helpers.js",
		_LOC+"js/app/config.js", //load app config values
		_LOC+"js/mddaf-sidemenu.js",
		_LOC+"js/indicator.js",
		_LOC+"js/mddaf-router.js",
		//App Specific
		_LOC+"js/app/h5m-app.js",
		_LOC+"js/app/views/bloopgen.js",
		_LOC+"js/app/views/main.js",

	];

	head.load(_LOC+"lib/js/velocity.min.js", function() {
	    // Call a function when done
	    _v = Velocity;
	});

	head.load(deps, function() {
	    // Call a function when done
	    _jQ = jQuery.noConflict();
	    _jS =_jQ.jStorage;
	    _m = MDDAF;

   		$ang = angular.module('MDDAF',[]); //app object
		$ang.config(function($interpolateProvider){
			$interpolateProvider.startSymbol('[[').endSymbol(']]');
		});

		//load promise polyfil if needed
	    if(typeof(Promise) === "undefined"){
			head.load(_LOC+"lib/js/promise.js");
	    }

	   head.ready(function(){
			$ang.controller("sideMenuCtrl",MDDAF.$sidemenu.ctrl);
		   _m.app.init();
		   head.load("cordova.js",function(){
               var handleBackBtn = function(ev){
                   ev.preventDefault();
                   if(MDDAF.app.bloopgen.listShowing){
                       MDDAF.app.bloopgen.hideBiList();
					   return;
                   }
                   if(!MDDAF.app.bloopgen.listShowing && MDDAF.app.currentView == "bloopgen"){
	                   _m.Router.nav.go2("main");
					   return;
				   }
				   if(MDDAF.app.currentView == "main"){
					   navigator.app.exitApp();
				   }
               }

               //backbutton bindings
               document.addEventListener("backbutton",handleBackBtn,false);
		   });
	   })

	});
})();
