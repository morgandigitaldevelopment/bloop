(function(){
    MDDAF.Router = {
        viewCache:{
            raw:{},
            rendered:{}
        },
        nav:{
            go2:function(go2,render,renderData){

                var $router = MDDAF.Router;

                if(!render){
                  render = false;
                }
                _jQ("#loader").fadeIn(function(){
                    //load html view with mustache
                    $router.getViewHTML(go2,function(viewTmp){
                      var $sidemenu = _ang.sidemenu;
                        if(render === true){
                            $router.renderContent(go2,viewTmp,renderData);
                        }else{
                            $router.updateContent(go2,viewTmp);
                        }

                        $sidemenu.hide();
                        $sidemenu.$apply();
                    });
                });
            }
        },
        loadView:function(view){

            //return function width
            return function(viewJS,cb){
                head.load(viewJS,cb);
            }
        },
        updateContent:function(view,renderedView){
            //get content container
            var content = document.getElementById('content');
                content.innerHTML = renderedView;
                _m.$indicator.hide();
                _jQ("#loader").fadeOut();
                _m.app[view].init();
                MDDAF.app.currentView = view;
        },
        renderContent:function(view,viewTmp,data){
            var $router = MDDAF.Router;

            if(!data){
              MDDAF.app[view].loadData(function(renderData){
                  $router.updateContent(view,Mustache.render(viewTmp,renderData));
              });
            }else{
                  $router.updateContent(view,Mustache.render(viewTmp,data));
            }

        },
        getViewHTML:function(view,cb){

            var good = function(data){
                MDDAF.Router.viewCache.raw[view] = data;
                MDDAF.Router.currentView = view;

                cb(data);
                // MDDAF.$indicator.hide();
            },
            bad = function(){
                _m.$indicator.error('ERROR:LOAD VIEW FAILED');
                MDDAF.$indicator.hide();
                _jQ("#loader").fadeOut();
            }
            _jQ.ajax({
        		  url: _VIEWLOC+view+".html",
        		  dataType: 'html',
        		  success: good,
        		  error: bad
        		});
        }
    }
})();
