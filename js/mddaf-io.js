(function(){
    MDDAF.io = {
        run:function(endpt,args,body,success,fail,method){

              var options = {
                  type: (method)?method:"GET",
                  url: endpt,
                  data:JSON.stringify(body),
                  dataType: 'json',
                  success: success,
                  error: fail
              };
             _jQ.ajax(options);
        }
    };
})();
